package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type ProductiveInvoiceUsecase interface {
	CreateProductiveInvoice(ctx context.Context, productiveInvoiceRegistration domain.ApplicationProductiveInvoice) (domain.ProductiveInvoice, *domain.ResponseErr)
	GetProductiveInvoices(ctx context.Context, criteria domain.ProductiveInvoiceSearch, pageSize int) ([]domain.ProductiveInvoice, *domain.ResponseErr)
}

func NewProductiveInvoiceUsecase(
	productiveInvoiceRepo repository.ProductiveInvoiceRepository,
) ProductiveInvoiceUsecase {
	return &productiveInvoiceUsecase{
		encryption:            encryption.NewBcryptEncryption(),
		productiveInvoiceRepo: productiveInvoiceRepo,
	}
}

type productiveInvoiceUsecase struct {
	encryption            encryption.Encription
	productiveInvoiceRepo repository.ProductiveInvoiceRepository
}

func (u *productiveInvoiceUsecase) CreateProductiveInvoice(ctx context.Context, productiveInvoiceRegistration domain.ApplicationProductiveInvoice) (productiveInvoice domain.ProductiveInvoice, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	productiveInvoice = productiveInvoiceRegistration.TransformToProductiveInvoice()
	productiveInvoice.CreatedAt = currentTime
	productiveInvoice.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(productiveInvoice)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	productiveInvoiceID, err := u.productiveInvoiceRepo.CreateProductiveInvoice(ctx, productiveInvoice)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	productiveInvoice.ID = int(productiveInvoiceID)
	return
}

func (u *productiveInvoiceUsecase) GetProductiveInvoices(ctx context.Context, criteria domain.ProductiveInvoiceSearch, pageSize int) (productiveInvoices []domain.ProductiveInvoice, apiErr *domain.ResponseErr) {
	productiveInvoices, err := u.productiveInvoiceRepo.ListProductiveInvoice(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
