package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type ConventionalInvoiceUsecase interface {
	CreateConventionalInvoice(ctx context.Context, conventionalInvoiceRegistration domain.ApplicationConventionalInvoice) (domain.ConventionalInvoice, *domain.ResponseErr)
	GetConventionalInvoices(ctx context.Context, criteria domain.ConventionalInvoiceSearch, pageSize int) ([]domain.ConventionalInvoice, *domain.ResponseErr)
}

func NewConventionalInvoiceUsecase(
	conventionalInvoiceRepo repository.ConventionalInvoiceRepository,
) ConventionalInvoiceUsecase {
	return &conventionalInvoiceUsecase{
		encryption:              encryption.NewBcryptEncryption(),
		conventionalInvoiceRepo: conventionalInvoiceRepo,
	}
}

type conventionalInvoiceUsecase struct {
	encryption              encryption.Encription
	conventionalInvoiceRepo repository.ConventionalInvoiceRepository
}

func (u *conventionalInvoiceUsecase) CreateConventionalInvoice(ctx context.Context, conventionalInvoiceRegistration domain.ApplicationConventionalInvoice) (conventionalInvoice domain.ConventionalInvoice, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	conventionalInvoice = conventionalInvoiceRegistration.TransformToConventionalInvoice()
	conventionalInvoice.CreatedAt = currentTime
	conventionalInvoice.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(conventionalInvoice)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	conventionalInvoiceID, err := u.conventionalInvoiceRepo.CreateConventionalInvoice(ctx, conventionalInvoice)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	conventionalInvoice.ID = int(conventionalInvoiceID)
	return
}

func (u *conventionalInvoiceUsecase) GetConventionalInvoices(ctx context.Context, criteria domain.ConventionalInvoiceSearch, pageSize int) (conventionalInvoices []domain.ConventionalInvoice, apiErr *domain.ResponseErr) {
	conventionalInvoices, err := u.conventionalInvoiceRepo.ListConventionalInvoice(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
