package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type OsfUsecase interface {
	CreateOsf(ctx context.Context, osfRegistration domain.ApplicationOsf) (domain.Osf, *domain.ResponseErr)
	GetOsfs(ctx context.Context, criteria domain.OsfSearch, pageSize int) ([]domain.Osf, *domain.ResponseErr)
}

func NewOsfUsecase(
	osfRepo repository.OsfRepository,
) OsfUsecase {
	return &osfUsecase{
		encryption: encryption.NewBcryptEncryption(),
		osfRepo:    osfRepo,
	}
}

type osfUsecase struct {
	encryption encryption.Encription
	osfRepo    repository.OsfRepository
}

func (u *osfUsecase) CreateOsf(ctx context.Context, osfRegistration domain.ApplicationOsf) (osf domain.Osf, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	osf = osfRegistration.TransformToOsf()
	osf.CreatedAt = currentTime
	osf.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(osf)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	osfID, err := u.osfRepo.CreateOsf(ctx, osf)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	osf.ID = int(osfID)
	return
}

func (u *osfUsecase) GetOsfs(ctx context.Context, criteria domain.OsfSearch, pageSize int) (osfs []domain.Osf, apiErr *domain.ResponseErr) {
	osfs, err := u.osfRepo.ListOsf(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
