package usecase

import (
	"context"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/token"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type AuthenticationUsecase interface {
	Login(ctx context.Context, username, password string) (token string, err *domain.ResponseErr)
	ValidateLogin(token string) (credential domain.Credential, err *domain.ResponseErr)
}

func NewAuthenticationUsecase(
	userRepo repository.UserRepository,
) AuthenticationUsecase {
	return &authenticationUsecase{
		userRepo:   userRepo,
		encryption: encryption.NewBcryptEncryption(),
	}
}

type authenticationUsecase struct {
	userRepo   repository.UserRepository
	encryption encryption.Encription
}

func (a *authenticationUsecase) Login(ctx context.Context, username, password string) (jwtToken string, apiErr *domain.ResponseErr) {
	creteria := domain.UserSearch{
		Username: username,
	}

	pageSize := 1
	users, err := a.userRepo.ListUser(ctx, creteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
		return
	}

	if len(users) == 0 {
		apiErr = &domain.ResponseErr{
			Code:    domain.AuthenticationError.Error(),
			Message: "Username is not found",
		}
		return
	}

	user := users[0]
	isValid := a.encryption.Compare([]byte(user.Password), []byte(password))
	if !isValid {
		apiErr = &domain.ResponseErr{
			Code:    domain.AuthenticationError.Error(),
			Message: "Password is not match",
		}
		return
	}

	credential := domain.Credential{
		UserID:   user.ID,
		Email:    user.Email,
		Username: user.Username,
	}

	jwtToken, err = token.EncodeToken(credential)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}

func (a *authenticationUsecase) ValidateLogin(jwtToken string) (credential domain.Credential, apiErr *domain.ResponseErr) {
	credential, err := token.DecodeToken(jwtToken)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.AuthenticationError.Error(),
			Message: err.Error(),
		}
	}

	return
}
