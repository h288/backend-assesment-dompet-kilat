package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type SbnUsecase interface {
	CreateSbn(ctx context.Context, sbnRegistration domain.ApplicationSbn) (domain.Sbn, *domain.ResponseErr)
	GetSbns(ctx context.Context, criteria domain.SbnSearch, pageSize int) ([]domain.Sbn, *domain.ResponseErr)
}

func NewSbnUsecase(
	sbnRepo repository.SbnRepository,
) SbnUsecase {
	return &sbnUsecase{
		encryption: encryption.NewBcryptEncryption(),
		sbnRepo:    sbnRepo,
	}
}

type sbnUsecase struct {
	encryption encryption.Encription
	sbnRepo    repository.SbnRepository
}

func (u *sbnUsecase) CreateSbn(ctx context.Context, sbnRegistration domain.ApplicationSbn) (sbn domain.Sbn, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	sbn = sbnRegistration.TransformToSbn()
	sbn.CreatedAt = currentTime
	sbn.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(sbn)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	sbnID, err := u.sbnRepo.CreateSbn(ctx, sbn)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	sbn.ID = int(sbnID)
	return
}

func (u *sbnUsecase) GetSbns(ctx context.Context, criteria domain.SbnSearch, pageSize int) (sbns []domain.Sbn, apiErr *domain.ResponseErr) {
	sbns, err := u.sbnRepo.ListSbn(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
