package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type UserUsecase interface {
	CreateUser(ctx context.Context, userRegistration domain.UserRegistration) (domain.User, *domain.ResponseErr)
	GetUsers(ctx context.Context, criteria domain.UserSearch, pageSize int) ([]domain.User, *domain.ResponseErr)
}

func NewUserUsecase(
	userRepo repository.UserRepository,
) UserUsecase {
	return &userUsecase{
		encryption: encryption.NewBcryptEncryption(),
		userRepo:   userRepo,
	}
}

type userUsecase struct {
	encryption encryption.Encription
	userRepo   repository.UserRepository
}

func (u *userUsecase) CreateUser(ctx context.Context, userRegistration domain.UserRegistration) (user domain.User, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	user = userRegistration.TransformToUser()
	user.CreatedAt = currentTime
	user.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(user)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	hashed, err := u.encryption.Encrypt([]byte(user.Password))
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
		return
	}
	user.Password = hashed

	//2. check duplicate email and username
	criteria := domain.UserSearch{
		Email:                  user.Email,
		Username:               user.Username,
		SearchWithAndOperation: false,
	}
	exists, err := u.userRepo.ValidateUserExists(ctx, criteria)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
		return
	}
	if exists {
		apiErr = &domain.ResponseErr{
			Code:    domain.DuplicateDataError.Error(),
			Message: "Username and Email already exists",
		}
		return
	}

	//3. save to db
	userID, err := u.userRepo.CreateUser(ctx, user)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	user.ID = int(userID)
	return
}

func (u *userUsecase) GetUsers(ctx context.Context, criteria domain.UserSearch, pageSize int) (users []domain.User, apiErr *domain.ResponseErr) {
	users, err := u.userRepo.ListUser(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
