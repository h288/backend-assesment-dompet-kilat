package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type ReksadanaUsecase interface {
	CreateReksadana(ctx context.Context, reksadanaRegistration domain.ApplicationReksadana) (domain.Reksadana, *domain.ResponseErr)
	GetReksadanas(ctx context.Context, criteria domain.ReksadanaSearch, pageSize int) ([]domain.Reksadana, *domain.ResponseErr)
}

func NewReksadanaUsecase(
	reksadanaRepo repository.ReksadanaRepository,
) ReksadanaUsecase {
	return &reksadanaUsecase{
		encryption:    encryption.NewBcryptEncryption(),
		reksadanaRepo: reksadanaRepo,
	}
}

type reksadanaUsecase struct {
	encryption    encryption.Encription
	reksadanaRepo repository.ReksadanaRepository
}

func (u *reksadanaUsecase) CreateReksadana(ctx context.Context, reksadanaRegistration domain.ApplicationReksadana) (reksadana domain.Reksadana, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	reksadana = reksadanaRegistration.TransformToReksadana()
	reksadana.CreatedAt = currentTime
	reksadana.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(reksadana)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	reksadanaID, err := u.reksadanaRepo.CreateReksadana(ctx, reksadana)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	reksadana.ID = int(reksadanaID)
	return
}

func (u *reksadanaUsecase) GetReksadanas(ctx context.Context, criteria domain.ReksadanaSearch, pageSize int) (reksadanas []domain.Reksadana, apiErr *domain.ResponseErr) {
	reksadanas, err := u.reksadanaRepo.ListReksadana(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
