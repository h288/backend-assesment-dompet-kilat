package usecase

import (
	"context"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/encryption"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper/validation"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
)

type FinancingUsecase interface {
	CreateFinancing(ctx context.Context, financingRegistration domain.ApplicationFinancing) (domain.Financing, *domain.ResponseErr)
	GetFinancings(ctx context.Context, criteria domain.FinancingSearch, pageSize int) ([]domain.Financing, *domain.ResponseErr)
}

func NewFinancingUsecase(
	financingRepo repository.FinancingRepository,
) FinancingUsecase {
	return &financingUsecase{
		encryption:    encryption.NewBcryptEncryption(),
		financingRepo: financingRepo,
	}
}

type financingUsecase struct {
	encryption    encryption.Encription
	financingRepo repository.FinancingRepository
}

func (u *financingUsecase) CreateFinancing(ctx context.Context, financingRegistration domain.ApplicationFinancing) (financing domain.Financing, apiErr *domain.ResponseErr) {

	currentTime := time.Now()
	financing = financingRegistration.TransformToFinancing()
	financing.CreatedAt = currentTime
	financing.UpdatedAt = currentTime

	//1. validation
	validation, apiErr := validation.NewStructValidation(financing)
	if apiErr != nil {
		return
	}
	apiErr = validation.Validate()
	if apiErr != nil {
		return
	}

	//3. save to db
	financingID, err := u.financingRepo.CreateFinancing(ctx, financing)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	financing.ID = int(financingID)
	return
}

func (u *financingUsecase) GetFinancings(ctx context.Context, criteria domain.FinancingSearch, pageSize int) (financings []domain.Financing, apiErr *domain.ResponseErr) {
	financings, err := u.financingRepo.ListFinancing(ctx, criteria, pageSize)
	if err != nil {
		apiErr = &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		}
	}

	return
}
