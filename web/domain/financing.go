package domain

import (
	"time"
)

type Financing struct {
	ID        int       `json:"id"`
	Name      string    `json:"name" validate:"minlength=1"`
	Count     int       `json:"count" validate:"min=1"`
	Sub       *string   `json:"sub"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ApplicationFinancing struct {
	Name  string  `json:"name"`
	Count int     `json:"count"`
	Sub   *string `json:"sub"`
}

func (a *ApplicationFinancing) TransformToFinancing() Financing {
	f := Financing{
		Name:  a.Name,
		Count: a.Count,
		Sub:   a.Sub,
	}

	return f
}

type FinancingSearch struct {
	LastID int    `json:"last_id"`
	Name   string `json:"name"`
	Count  int    `json:"count"`
	Sub    string `json:"sub"`
}
