package domain

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

var APPLICATION_NAME = "DOMPET-KILAT"

type Credential struct {
	jwt.StandardClaims
	UserID   int    `json:"user_id"`
	Email    string `json:"email"`
	Username string `json:"username"`
}

func NewCredential(userID int, email, username string) Credential {
	return Credential{
		jwt.StandardClaims{
			Issuer:   APPLICATION_NAME,
			IssuedAt: time.Now().Unix(),
		},
		userID,
		email,
		username,
	}
}

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
