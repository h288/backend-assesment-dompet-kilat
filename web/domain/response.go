package domain

import (
	"errors"
	"net/http"
	"time"
)

type ResponseSuccess struct {
	Timestamp time.Time   `json:"time_stamp"`
	Data      interface{} `json:"data"`
}

type ResponseErr struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

var (
	InternalServerError = errors.New("InternalServerError")
	InvalidDataError    = errors.New("InvalidDataError")
	DuplicateDataError  = errors.New("DataDuplicateError")
	AuthenticationError = errors.New("AuthenticationError")
)

var mapErrToHTTPStatus = map[error]int{
	InternalServerError: http.StatusInternalServerError,
	InvalidDataError:    http.StatusBadRequest,
	DuplicateDataError:  http.StatusBadRequest,
	AuthenticationError: http.StatusUnauthorized,
}

func GetHttpStatus(err error) (httpStatus int) {
	httpStatus = mapErrToHTTPStatus[err]

	if httpStatus == 0 {
		httpStatus = http.StatusInternalServerError
	}

	return
}
