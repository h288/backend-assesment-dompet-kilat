package domain

import (
	"time"
)

type Reksadana struct {
	ID        int       `json:"id"`
	Name      string    `json:"name" validate:"minlength=1"`
	Amount    int       `json:"amount" validate:"min=1"`
	Return    int       `json:"return"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ApplicationReksadana struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Return int    `json:"return"`
}

func (a *ApplicationReksadana) TransformToReksadana() Reksadana {
	return Reksadana{
		Name:   a.Name,
		Amount: a.Amount,
		Return: a.Return,
	}

}

type ReksadanaSearch struct {
	LastID int    `json:"last_id"`
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Return int    `json:"return"`
}
