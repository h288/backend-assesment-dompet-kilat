package domain

import (
	"time"
)

type ProductiveInvoice struct {
	ID        int       `json:"id"`
	Name      string    `json:"name" validate:"minlength=1"`
	Amount    int       `json:"amount" validate:"min=1"`
	Tenor     int       `json:"tenor" validate:"min=1"`
	Grade     string    `json:"grade" validate:"minLength=1"`
	Rate      int       `json:"rate" validate:"min=1"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ApplicationProductiveInvoice struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Grade  string `json:"grade"`
	Rate   int    `json:"rate"`
}

func (a *ApplicationProductiveInvoice) TransformToProductiveInvoice() ProductiveInvoice {
	f := ProductiveInvoice{
		Name:   a.Name,
		Amount: a.Amount,
		Tenor:  a.Tenor,
		Grade:  a.Grade,
		Rate:   a.Rate,
	}

	return f
}

type ProductiveInvoiceSearch struct {
	LastID int    `json:"last_id"`
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Grade  string `json:"grade"`
	Rate   int    `json:"rate"`
}
