package domain

import "time"

// user struct is    representation of a user data
type User struct {
	ID        int       `json:"id"`
	Username  string    `json:"username" validate:"min=8"`
	Email     string    `json:"email" validate:"email"`
	Password  string    `json:"-" validate:"password"`
	CreatedAt time.Time `json:"created_at" validate:"nonEmpty"`
	UpdatedAt time.Time `json:"updated_at" validate:"nonEmpty"`
}

// userRegistration struct is representation of a new user registration data
type UserRegistration struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u *UserRegistration) TransformToUser() User {
	return User{
		Username: u.Username,
		Email:    u.Email,
		Password: u.Password,
	}
}

// userSearch struct is representation of searching creteria for user
type UserSearch struct {
	LastID                 int    `json:"last_id"`
	Username               string `json:"username"`
	Email                  string `json:"email"`
	SearchWithAndOperation bool   `json:"search_with_and_operation"`
}
