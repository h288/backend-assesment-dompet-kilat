package domain

import (
	"time"
)

type Sbn struct {
	ID        int       `json:"id"`
	Name      string    `json:"name" validate:"minlength=1"`
	Amount    int       `json:"amount" validate:"min=1"`
	Tenor     int       `json:"tenor" validate:"min=1"`
	Type      string    `json:"type" validate:"minLength=1"`
	Rate      int       `json:"rate" validate:"min=1"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ApplicationSbn struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Type   string `json:"type"`
	Rate   int    `json:"rate"`
}

func (a *ApplicationSbn) TransformToSbn() Sbn {
	f := Sbn{
		Name:   a.Name,
		Amount: a.Amount,
		Tenor:  a.Tenor,
		Type:   a.Type,
		Rate:   a.Rate,
	}

	return f
}

type SbnSearch struct {
	LastID int    `json:"last_id"`
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Type   string `json:"type"`
	Rate   int    `json:"rate"`
}
