package token

import (
	"errors"
	"os"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/config"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = []byte(os.Getenv(config.JWT_SECRET))

func EncodeToken(credential domain.Credential) (string, error) {
	var token string
	unSingnedToken := jwt.NewWithClaims(JWT_SIGNING_METHOD, credential)
	token, err := unSingnedToken.SignedString(JWT_SIGNATURE_KEY)
	if err != nil {
		return token, err
	}
	return token, nil
}

func DecodeToken(token string) (domain.Credential, error) {
	var credential domain.Credential

	jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		method, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok || method != JWT_SIGNING_METHOD {
			err := errors.New("Token method is not match")
			return credential, err
		}

		return JWT_SIGNATURE_KEY, nil
	})

	if err != nil {
		return credential, err
	}

	claims, ok := jwtToken.Claims.(jwt.MapClaims)
	if !ok || !jwtToken.Valid {
		return credential, err
	}

	email, ok := claims["email"].(string)
	if ok {
		credential.Email = email
	}
	userID, ok := claims["user_id"].(int)
	if ok {
		credential.UserID = userID
	}

	username, ok := claims["username"].(string)
	if ok {
		credential.Username = username
	}

	return credential, nil
}
