package helper

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

func HTTPResponseErr(w http.ResponseWriter, res domain.ResponseErr) {
	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	httpStatus := domain.GetHttpStatus(err)
	send(w, httpStatus, payload)
}

func HTTPResponseSuccess(w http.ResponseWriter, httpStatus int, data interface{}) {
	res := domain.ResponseSuccess{
		Timestamp: time.Now(),
		Data:      data,
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	send(w, httpStatus, payload)
}

func send(w http.ResponseWriter, httpStatus int, payload []byte) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(httpStatus)

	if _, err := w.Write(payload); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
