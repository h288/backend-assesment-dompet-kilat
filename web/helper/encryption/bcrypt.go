package encryption

import "golang.org/x/crypto/bcrypt"

type Encription interface {
	Encrypt(pass []byte) (string, error)
	Compare(hashedPassword, password []byte) bool
}

func NewBcryptEncryption() Encription {
	return &bcryptEncription{
		Cost: bcrypt.DefaultCost,
	}
}

type bcryptEncription struct {
	Cost int
}

func (b *bcryptEncription) Encrypt(pass []byte) (hashed string, err error) {
	hashedInByte, err := bcrypt.GenerateFromPassword(pass, b.Cost)
	hashed = string(hashedInByte)
	return
}

func (b *bcryptEncription) Compare(hashedPassword, password []byte) (valid bool) {
	err := bcrypt.CompareHashAndPassword(hashedPassword, password)
	if err != nil {
		return
	}

	valid = true
	return
}
