package validation

import (
	"reflect"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

/*
* StructValidation is representation of a struct model data
 */
type StructValidation interface {
	Validate() (err *domain.ResponseErr)
}

func NewStructValidation(s interface{}) (StructValidation, *domain.ResponseErr) {
	// initilize rule support and type data support
	initRuleSupport()

	fieldValidations := []fieldValidation{}

	reflectValue := reflect.ValueOf(s)
	reflectType := reflectValue.Type()

	if reflectType.Kind() != reflect.Struct {
		return nil, &domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: "Failed construct validation, cause value is not a struct",
		}
	}

	for i := 0; i < reflectValue.NumField(); i++ {
		fieldValidation := NewFieldValidation(
			reflectValue.Field(i),
			reflectType.Field(i),
		)

		fieldValidations = append(fieldValidations, fieldValidation)
	}
	return &structValidation{
		fields: fieldValidations,
	}, nil
}

type structValidation struct {
	fields []fieldValidation
}

func (s *structValidation) Validate() (err *domain.ResponseErr) {
	for _, field := range s.fields {
		if err := field.validate(); err != nil {
			return &domain.ResponseErr{
				Code:    domain.InvalidDataError.Error(),
				Message: err.Error(),
			}
		}
	}

	return nil
}
