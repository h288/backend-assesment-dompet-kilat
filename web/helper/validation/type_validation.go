package validation

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
)

/*
* TypeValidation is representation of validation rule from field validation
* based on field validation tag and type data
 */
type typeValidation interface {
	Validate(reflectValue reflect.Value, structField reflect.StructField) (err error)
}

func NewTypeValidation(
	rule string,
	constraint string,
	reflectValue reflect.Value,
	reflectType reflect.StructField,
) typeValidation {
	mapKind, ok := mapRuleSupport[rule]
	if !ok {
		return nil
	}

	_, ok = mapKind[reflectType.Type.Kind()]
	if !ok {
		return nil
	}

	switch rule {
	case VALIDATION_MIN:
		return &minValidation{min: constraint}
	case VALIDATION_MIN_LENGTH:
		return &minLengthValidation{min: constraint}
	case VALIDATION_EMAIL:
		return &emailValidation{}
	case VALIDATION_PASSWORD:
		return &passwordValidation{}
	default:
		return nil
	}
}

type minValidation struct {
	min string
}

func (m *minValidation) Validate(reflectValue reflect.Value, structField reflect.StructField) (err error) {
	messageFormat := "Min validation failed, for constraint : %d and value : %v"

	switch structField.Type.Kind() {
	case reflect.Int:
		constraintVal, err := strconv.ParseInt(m.min, 10, 0)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(int)
		if val < int(constraintVal) {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Int8:
		constraintVal, err := strconv.ParseInt(m.min, 10, 8)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(int)
		if val < int(constraintVal) {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Int16:
		constraintVal, err := strconv.ParseInt(m.min, 10, 16)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(int16)
		if val < int16(constraintVal) {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Int32:
		constraintVal, err := strconv.ParseInt(m.min, 10, 32)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(int32)
		if val < int32(constraintVal) {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Int64:
		constraintVal, err := strconv.ParseInt(m.min, 10, 64)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(int64)
		if val < constraintVal {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Float32:
		constraintVal, err := strconv.ParseFloat(m.min, 32)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(float32)
		if val < float32(constraintVal) {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	case reflect.Float64:
		constraintVal, err := strconv.ParseFloat(m.min, 64)
		if err != nil {
			return nil
		}

		val := reflectValue.Interface().(float64)
		if val < constraintVal {
			return errors.New(fmt.Sprint(messageFormat, m.min, val))
		}
	default:
		return nil
	}

	return nil
}

type minLengthValidation struct {
	min string
}

func (m *minLengthValidation) Validate(value reflect.Value, structField reflect.StructField) (err error) {
	messageFormat := "Min validation failed, for constraint : %d and value : %v"

	constraintVal, err := strconv.Atoi(m.min)
	if err != nil {
		return nil
	}

	if (structField.Type.Kind() == reflect.Array) ||
		(structField.Type.Kind() == reflect.Map) ||
		(structField.Type.Kind() == reflect.String) ||
		(structField.Type.Kind() == reflect.Slice) ||
		(structField.Type.Kind() == reflect.Chan) {
		if value.Len() < constraintVal {
			return errors.New(fmt.Sprint(messageFormat, m.min, value.Interface()))
		}
	}

	return nil
}

type emailValidation struct{}

func (e *emailValidation) Validate(value reflect.Value, structField reflect.StructField) (err error) {
	messageFormat := "Email validation failed, value : %v"

	emailRegex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	if !emailRegex.MatchString(value.String()) {
		return errors.New(fmt.Sprint(messageFormat, value.Interface()))
	}

	return nil
}

type passwordValidation struct{}

func (e *passwordValidation) Validate(value reflect.Value, structField reflect.StructField) (err error) {
	messageFormat := "Password validation failed, value : %v"

	lowerRegex := regexp.MustCompile("^(?:.*[a-z].*)$")
	upperRegex := regexp.MustCompile("^(?:.*[A-Z].*)$")
	numberRegex := regexp.MustCompile("^(?:.*[0-9].*)$")
	symbolRegex := regexp.MustCompile("^(?:.*[-!$%#^&*()_+|~=`{}\\[\\]:\";'<>?,.\\/].*)$")

	if !(lowerRegex.MatchString(value.String())) ||
		!(upperRegex.MatchString(value.String())) ||
		!(numberRegex.MatchString(value.String())) ||
		!symbolRegex.MatchString(value.String()) {
		return errors.New(fmt.Sprint(messageFormat, value.Interface()))
	}

	return nil
}
