package validation

import (
	"reflect"
	"sync"
)

const (
	TAG_NAME = "validate"

	VALIDATION_EMAIL      = "email"
	VALIDATION_PASSWORD   = "password"
	VALIDATION_MIN        = "min"
	VALIDATION_MIN_LENGTH = "minlength"
)

var onceValidation sync.Once

var mapRuleSupport map[string]map[reflect.Kind]bool

func initRuleSupport() {
	onceValidation.Do(func() {
		if mapRuleSupport == nil {
			mapRuleSupport = map[string]map[reflect.Kind]bool{
				VALIDATION_EMAIL: map[reflect.Kind]bool{
					reflect.String: true,
				},
				VALIDATION_PASSWORD: map[reflect.Kind]bool{
					reflect.String: true,
				},
				VALIDATION_MIN: map[reflect.Kind]bool{
					reflect.Int:     true,
					reflect.Int8:    true,
					reflect.Int16:   true,
					reflect.Int32:   true,
					reflect.Int64:   true,
					reflect.Float32: true,
					reflect.Float64: true,
				},
				VALIDATION_MIN_LENGTH: map[reflect.Kind]bool{
					reflect.Array:  true,
					reflect.Slice:  true,
					reflect.String: true,
					reflect.Map:    true,
				},
			}
		}
	})
}
