package validation

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMinValidationNormalFailedCaseString(t *testing.T) {
	type Dummy struct {
		Name string `validate:"minlength=1"`
	}

	d := Dummy{Name: ""}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.NotNil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalSuccessCaseString(t *testing.T) {
	type Dummy struct {
		Name string `validate:"minlength=1"`
	}

	d := Dummy{Name: "A"}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalFailedCaseArray(t *testing.T) {
	type Dummy struct {
		Name []string `validate:"minlength=1"`
	}

	d := Dummy{Name: []string{}}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.NotNil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalSuccessCaseArray(t *testing.T) {
	type Dummy struct {
		Name []string `validate:"minlength=1"`
	}

	d := Dummy{Name: []string{"A"}}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalFailedCaseMap(t *testing.T) {
	type Dummy struct {
		Name map[string]interface{} `validate:"minlength=1"`
	}

	d := Dummy{Name: map[string]interface{}{}}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.NotNil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalSuccessCaseMap(t *testing.T) {
	type Dummy struct {
		Name map[string]interface{} `validate:"minlength=1"`
	}

	d := Dummy{Name: map[string]interface{}{"A": "A"}}
	validation, _ := NewStructValidation(d)

	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min length validation with data : %v",
			err,
			d,
		)
	}
}
