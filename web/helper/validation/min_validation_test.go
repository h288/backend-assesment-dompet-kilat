package validation

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMinValidationNormalFailedCase(t *testing.T) {
	type Dummy struct {
		Min int `validate:"min=1"`
	}

	d := Dummy{Min: 0}
	validation, _ := NewStructValidation(d)
	err := validation.Validate()
	if !assert.NotNil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationNormalSuccessCase(t *testing.T) {
	type Dummy struct {
		Min int `validate:"min=1"`
	}

	d := Dummy{Min: 2}
	validation, _ := NewStructValidation(d)
	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationInvalidFormatTag(t *testing.T) {
	type Dummy struct {
		Min int `validate:"min="`
	}

	d := Dummy{Min: 2}
	validation, _ := NewStructValidation(d)
	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min validation with data : %v",
			err,
			d,
		)
	}
}

func TestMinValidationInvalidConstraintTag(t *testing.T) {
	type Dummy struct {
		Min int `validate:"min=c"`
	}

	d := Dummy{Min: 2}
	validation, _ := NewStructValidation(d)
	err := validation.Validate()
	if !assert.Nil(t, err) {
		t.Errorf(
			"Testcase failed! expected get domain error, get : %v for min validation with data : %v",
			err,
			d,
		)
	}
}
