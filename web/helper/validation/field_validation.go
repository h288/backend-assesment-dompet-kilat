package validation

import (
	"fmt"
	"reflect"
	"strings"
)

func NewFieldValidation(
	reflectValue reflect.Value,
	relfectType reflect.StructField,
) fieldValidation {
	typeValidations := []typeValidation{}

	tag := relfectType.Tag.Get(TAG_NAME)
	ruleStrings := strings.Split(tag, ",")
	for _, ruleString := range ruleStrings {
		splitRuleString := strings.Split(ruleString, "=")

		ruleName := ""
		if len(splitRuleString) >= 1 {
			ruleName = splitRuleString[0]
		}
		ruleConstraint := ""
		if len(splitRuleString) >= 2 {
			ruleConstraint = splitRuleString[1]
		}

		typeValidation := NewTypeValidation(
			ruleName,
			ruleConstraint,
			reflectValue,
			relfectType,
		)

		if typeValidation != nil {
			typeValidations = append(typeValidations, typeValidation)
		}
	}

	return fieldValidation{
		reflectValue: reflectValue,
		reflectType:  relfectType,
		validations:  typeValidations,
	}
}

/*
* FieldValidation is representation of a field from a struct
 */
type fieldValidation struct {
	reflectValue reflect.Value
	reflectType  reflect.StructField
	validations  []typeValidation
}

func (f *fieldValidation) generateMessage(err error) error {
	return fmt.Errorf(
		"Validation failed for field  : %s, cuase : %s",
		f.reflectType.Name,
		err,
	)
}

func (f *fieldValidation) validate() error {
	for _, validation := range f.validations {
		if err := validation.Validate(f.reflectValue, f.reflectType); err != nil {
			return f.generateMessage(err)
		}
	}

	return nil
}
