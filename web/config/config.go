package config

const (
	MYSQL_URI      = "mysql_uri"
	MYSQL_USERNAME = "mysql_username"
	MYSQL_PASSWORD = "mysql_password"
	MYSQL_DATABASE = "mysql_database"
	MYSQL_PORT     = "mysql_port"

	WEB_PORT   = "web_port"
	JWT_SECRET = "jwt_secret"
)
