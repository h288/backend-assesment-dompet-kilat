package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type UserRepository interface {
	CreateUser(ctx context.Context, user domain.User) (int64, error)
	ListUser(ctx context.Context, createria domain.UserSearch, pageSize int) (users []domain.User, err error)
	ValidateUserExists(ctx context.Context, createria domain.UserSearch) (exists bool, err error)
}

const (
	queryExistUser = `
		SELECT
			count(users.id)
		FROM users 
	`
	querySelectUser = `
		SELECT
			users.id,
			users.username,
			users.email,
			users.password,
			users.created_at,
			users.updated_at
		FROM users
	`
	queryInsertUser = `
		INSERT INTO users (
			email, 
			username, 
			password,
			created_at,
			updated_at
		) VALUES (?, ?, ?, ?, ?)
	`
)

func NewUserMySQLRepostitory(db *sql.DB) UserRepository {
	return &userMySQLRepository{
		db: db,
	}
}

type userMySQLRepository struct {
	db *sql.DB
}

func (u *userMySQLRepository) CreateUser(ctx context.Context, user domain.User) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertUser,
		user.Email,
		user.Username,
		user.Password,
		user.CreatedAt,
		user.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *userMySQLRepository) ListUser(ctx context.Context, createria domain.UserSearch, pageSize int) (users []domain.User, err error) {
	users = []domain.User{}
	args := []interface{}{}

	condition := ""
	if createria.Email != "" {
		condition = " users.email = ? "
		args = append(args, createria.Email)
	}

	if createria.Username != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " users.username = ? "
		args = append(args, createria.Username)
	}

	query := querySelectUser + " WHERE " + condition + " AND users.id > ? " + "LIMIT " + strconv.Itoa(pageSize)
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}
	args = append(args, createria.LastID)

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	user := domain.User{}
	for rows.Next() {
		err = rows.Scan(&user.ID, &user.Email, &user.Username, &user.Password, &user.CreatedAt, &user.UpdatedAt)
		if err != nil {
			return
		}
		users = append(users, user)
	}

	return
}

func (u *userMySQLRepository) ValidateUserExists(ctx context.Context, createria domain.UserSearch) (exists bool, err error) {
	operation := " AND "
	if !createria.SearchWithAndOperation {
		operation = " OR "
	}

	condition := ""
	if createria.Email != "" {
		condition = " users.email = ? "
	}

	if createria.Username != "" {
		if condition != "" {
			condition = condition + operation
		}

		condition = condition + " users.username = ?"
	}

	query := queryExistUser + "WHERE" + condition + " LIMIT 1"

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	row := tx.QueryRowContext(ctx, query, createria.Email, createria.Username)

	var count int = 0
	errScan := row.Scan(&count)
	if errScan != nil && errScan == sql.ErrNoRows {
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		return
	}

	return count == 1, errScan
}
