package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type ReksadanaRepository interface {
	CreateReksadana(ctx context.Context, reksadana domain.Reksadana) (int64, error)
	ListReksadana(ctx context.Context, createria domain.ReksadanaSearch, pageSize int) (reksadanas []domain.Reksadana, err error)
}

const (
	querySelectReksadana = `
		SELECT
			reksadana.id,
			reksadana.name,
			reksadana.amount,
			reksadana.return,
			reksadana.created_at,
			reksadana.updated_at
		FROM reksadana
	`
	queryInsertReksadana = `
		INSERT INTO reksadana (
			reksadana.name,
			reksadana.amount,
			reksadana.return,
			reksadana.created_at,
			reksadana.updated_at
		) VALUES (?, ?, ?, ?, ?)
	`
)

func NewReksadanaMySQLRepostitory(db *sql.DB) ReksadanaRepository {
	return &reksadanaMySQLRepository{
		db: db,
	}
}

type reksadanaMySQLRepository struct {
	db *sql.DB
}

func (u *reksadanaMySQLRepository) CreateReksadana(ctx context.Context, reksadana domain.Reksadana) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertReksadana,
		reksadana.Name,
		reksadana.Amount,
		reksadana.Return,
		reksadana.CreatedAt,
		reksadana.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *reksadanaMySQLRepository) ListReksadana(ctx context.Context, createria domain.ReksadanaSearch, pageSize int) (reksadanas []domain.Reksadana, err error) {
	reksadanas = []domain.Reksadana{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " reksadana.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Amount != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " reksadana.amount = ? "
		args = append(args, createria.Amount)
	}

	if createria.Return != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " reksadana.return = ? "
		args = append(args, createria.Return)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " reksadana.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectReksadana
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	reksadana := domain.Reksadana{}
	for rows.Next() {
		err = rows.Scan(&reksadana.ID, &reksadana.Name, &reksadana.Amount, &reksadana.Return, &reksadana.CreatedAt, &reksadana.UpdatedAt)
		if err != nil {
			return
		}
		reksadanas = append(reksadanas, reksadana)
	}

	return
}
