package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type ProductiveInvoiceRepository interface {
	CreateProductiveInvoice(ctx context.Context, productiveInvoice domain.ProductiveInvoice) (int64, error)
	ListProductiveInvoice(ctx context.Context, createria domain.ProductiveInvoiceSearch, pageSize int) (productiveInvoices []domain.ProductiveInvoice, err error)
}

const (
	querySelectProductiveInvoice = `
		SELECT
			productive_invoices.id,
			productive_invoices.name,
			productive_invoices.amount,
			productive_invoices.tenor,
			productive_invoices.grade,
			productive_invoices.rate,
			productive_invoices.created_at,
			productive_invoices.updated_at
		FROM productive_invoices
	`
	queryInsertProductiveInvoice = `
		INSERT INTO productive_invoices (
			productive_invoices.name,
			productive_invoices.amount,
			productive_invoices.tenor,
			productive_invoices.grade,
			productive_invoices.rate,
			productive_invoices.created_at,
			productive_invoices.updated_at
		) VALUES (?, ?, ?, ?, ?, ?, ?)
	`
)

func NewProductiveInvoiceMySQLRepostitory(db *sql.DB) ProductiveInvoiceRepository {
	return &productiveInvoiceMySQLRepository{
		db: db,
	}
}

type productiveInvoiceMySQLRepository struct {
	db *sql.DB
}

func (u *productiveInvoiceMySQLRepository) CreateProductiveInvoice(ctx context.Context, productiveInvoice domain.ProductiveInvoice) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertProductiveInvoice,
		productiveInvoice.Name,
		productiveInvoice.Amount,
		productiveInvoice.Tenor,
		productiveInvoice.Grade,
		productiveInvoice.Rate,
		productiveInvoice.CreatedAt,
		productiveInvoice.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *productiveInvoiceMySQLRepository) ListProductiveInvoice(ctx context.Context, createria domain.ProductiveInvoiceSearch, pageSize int) (productiveInvoices []domain.ProductiveInvoice, err error) {
	productiveInvoices = []domain.ProductiveInvoice{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " productive_invoices.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Amount != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " productive_invoices.amount = ? "
		args = append(args, createria.Amount)
	}

	if createria.Tenor != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " productive_invoices.tenor = ? "
		args = append(args, createria.Tenor)
	}

	if createria.Grade != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " productive_invoices.grade = ? "
		args = append(args, createria.Grade)
	}

	if createria.Rate != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " productive_invoices.rate = ? "
		args = append(args, createria.Rate)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " productive_invoices.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectProductiveInvoice
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	productiveInvoice := domain.ProductiveInvoice{}
	for rows.Next() {
		err = rows.Scan(&productiveInvoice.ID, &productiveInvoice.Name, &productiveInvoice.Amount, &productiveInvoice.Tenor, &productiveInvoice.Grade, &productiveInvoice.Rate, &productiveInvoice.CreatedAt, &productiveInvoice.UpdatedAt)
		if err != nil {
			return
		}
		productiveInvoices = append(productiveInvoices, productiveInvoice)
	}

	return
}
