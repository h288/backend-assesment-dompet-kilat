package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type FinancingRepository interface {
	CreateFinancing(ctx context.Context, financing domain.Financing) (int64, error)
	ListFinancing(ctx context.Context, createria domain.FinancingSearch, pageSize int) (financings []domain.Financing, err error)
}

const (
	querySelectFinancing = `
		SELECT
			financings.id,
			financings.name,
			financings.count,
			financings.sub,
			financings.created_at,
			financings.updated_at
		FROM financings
	`
	queryInsertFinancing = `
		INSERT INTO financings (
			name,
			count,
			sub,
			created_at,
			updated_at
		) VALUES (?, ?, ?, ?, ?)
	`
)

func NewFinancingMySQLRepostitory(db *sql.DB) FinancingRepository {
	return &financingMySQLRepository{
		db: db,
	}
}

type financingMySQLRepository struct {
	db *sql.DB
}

func (u *financingMySQLRepository) CreateFinancing(ctx context.Context, financing domain.Financing) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertFinancing,
		financing.Name,
		financing.Count,
		financing.Sub,
		financing.CreatedAt,
		financing.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *financingMySQLRepository) ListFinancing(ctx context.Context, createria domain.FinancingSearch, pageSize int) (financings []domain.Financing, err error) {
	financings = []domain.Financing{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " financings.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Count != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " financings.count = ? "
		args = append(args, createria.Count)
	}

	if createria.Sub != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " financings.sub = ? "
		args = append(args, createria.Sub)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " financings.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectFinancing
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	financing := domain.Financing{}
	for rows.Next() {
		err = rows.Scan(&financing.ID, &financing.Name, &financing.Count, &financing.Sub, &financing.CreatedAt, &financing.UpdatedAt)
		if err != nil {
			return
		}
		financings = append(financings, financing)
	}

	return
}
