package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type SbnRepository interface {
	CreateSbn(ctx context.Context, sbn domain.Sbn) (int64, error)
	ListSbn(ctx context.Context, createria domain.SbnSearch, pageSize int) (sbns []domain.Sbn, err error)
}

const (
	querySelectSbn = `
		SELECT
			sbns.id,
			sbns.name,
			sbns.amount,
			sbns.tenor,
			sbns.type,
			sbns.rate,
			sbns.created_at,
			sbns.updated_at
		FROM sbns
	`
	queryInsertSbn = `
		INSERT INTO sbns (
			sbns.name,
			sbns.amount,
			sbns.tenor,
			sbns.type,
			sbns.rate,
			sbns.created_at,
			sbns.updated_at
		) VALUES (?, ?, ?, ?, ?, ?, ?)
	`
)

func NewSbnMySQLRepostitory(db *sql.DB) SbnRepository {
	return &sbnMySQLRepository{
		db: db,
	}
}

type sbnMySQLRepository struct {
	db *sql.DB
}

func (u *sbnMySQLRepository) CreateSbn(ctx context.Context, sbn domain.Sbn) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertSbn,
		sbn.Name,
		sbn.Amount,
		sbn.Tenor,
		sbn.Type,
		sbn.Rate,
		sbn.CreatedAt,
		sbn.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *sbnMySQLRepository) ListSbn(ctx context.Context, createria domain.SbnSearch, pageSize int) (sbns []domain.Sbn, err error) {
	sbns = []domain.Sbn{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " sbns.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Amount != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " sbns.amount = ? "
		args = append(args, createria.Amount)
	}

	if createria.Tenor != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " sbns.tenor = ? "
		args = append(args, createria.Tenor)
	}

	if createria.Type != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " sbns.type = ? "
		args = append(args, createria.Type)
	}

	if createria.Rate != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " sbns.rate = ? "
		args = append(args, createria.Rate)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " sbns.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectSbn
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	sbn := domain.Sbn{}
	for rows.Next() {
		err = rows.Scan(&sbn.ID, &sbn.Name, &sbn.Amount, &sbn.Tenor, &sbn.Type, &sbn.Rate, &sbn.CreatedAt, &sbn.UpdatedAt)
		if err != nil {
			return
		}
		sbns = append(sbns, sbn)
	}

	return
}
