package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type OsfRepository interface {
	CreateOsf(ctx context.Context, osf domain.Osf) (int64, error)
	ListOsf(ctx context.Context, createria domain.OsfSearch, pageSize int) (osfs []domain.Osf, err error)
}

const (
	querySelectOsf = `
		SELECT
			osfs.id,
			osfs.name,
			osfs.amount,
			osfs.tenor,
			osfs.grade,
			osfs.rate,
			osfs.created_at,
			osfs.updated_at
		FROM osfs
	`
	queryInsertOsf = `
		INSERT INTO osfs (
			osfs.name,
			osfs.amount,
			osfs.tenor,
			osfs.grade,
			osfs.rate,
			created_at,
			updated_at
		) VALUES (?, ?, ?, ?, ?, ?, ?)
	`
)

func NewOsfMySQLRepostitory(db *sql.DB) OsfRepository {
	return &osfMySQLRepository{
		db: db,
	}
}

type osfMySQLRepository struct {
	db *sql.DB
}

func (u *osfMySQLRepository) CreateOsf(ctx context.Context, osf domain.Osf) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertOsf,
		osf.Name,
		osf.Amount,
		osf.Tenor,
		osf.Grade,
		osf.Rate,
		osf.CreatedAt,
		osf.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *osfMySQLRepository) ListOsf(ctx context.Context, createria domain.OsfSearch, pageSize int) (osfs []domain.Osf, err error) {
	osfs = []domain.Osf{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " osfs.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Amount != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " osfs.amount = ? "
		args = append(args, createria.Amount)
	}

	if createria.Tenor != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " osfs.tenor = ? "
		args = append(args, createria.Tenor)
	}

	if createria.Grade != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " osfs.grade = ? "
		args = append(args, createria.Grade)
	}

	if createria.Rate != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " osfs.rate = ? "
		args = append(args, createria.Rate)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " osfs.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectOsf
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	osf := domain.Osf{}
	for rows.Next() {
		err = rows.Scan(&osf.ID, &osf.Name, &osf.Amount, &osf.Tenor, &osf.Grade, &osf.Rate, &osf.CreatedAt, &osf.UpdatedAt)
		if err != nil {
			return
		}
		osfs = append(osfs, osf)
	}

	return
}
