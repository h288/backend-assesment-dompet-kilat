package repository

import (
	"context"
	"database/sql"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
)

type ConventionalInvoiceRepository interface {
	CreateConventionalInvoice(ctx context.Context, conventionalInvoice domain.ConventionalInvoice) (int64, error)
	ListConventionalInvoice(ctx context.Context, createria domain.ConventionalInvoiceSearch, pageSize int) (conventionalInvoices []domain.ConventionalInvoice, err error)
}

const (
	querySelectConventionalInvoice = `
		SELECT
			conventional_invoices.id,
			conventional_invoices.name,
			conventional_invoices.amount,
			conventional_invoices.tenor,
			conventional_invoices.grade,
			conventional_invoices.rate,
			conventional_invoices.created_at,
			conventional_invoices.updated_at
		FROM conventional_invoices
	`
	queryInsertConventionalInvoice = `
		INSERT INTO conventional_invoices (
			conventional_invoices.name,
			conventional_invoices.amount,
			conventional_invoices.tenor,
			conventional_invoices.grade,
			conventional_invoices.rate,
			created_at,
			updated_at
		) VALUES (?, ?, ?, ?, ?, ?, ?)
	`
)

func NewConventionalInvoiceMySQLRepostitory(db *sql.DB) ConventionalInvoiceRepository {
	return &conventionalInvoiceMySQLRepository{
		db: db,
	}
}

type conventionalInvoiceMySQLRepository struct {
	db *sql.DB
}

func (u *conventionalInvoiceMySQLRepository) CreateConventionalInvoice(ctx context.Context, conventionalInvoice domain.ConventionalInvoice) (lastID int64, err error) {
	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	res, err := tx.ExecContext(
		ctx,
		queryInsertConventionalInvoice,
		conventionalInvoice.Name,
		conventionalInvoice.Amount,
		conventionalInvoice.Tenor,
		conventionalInvoice.Grade,
		conventionalInvoice.Rate,
		conventionalInvoice.CreatedAt,
		conventionalInvoice.UpdatedAt,
	)
	if err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Commit(); err != nil {
		return
	}

	return res.LastInsertId()
}

func (u *conventionalInvoiceMySQLRepository) ListConventionalInvoice(ctx context.Context, createria domain.ConventionalInvoiceSearch, pageSize int) (conventionalInvoices []domain.ConventionalInvoice, err error) {
	conventionalInvoices = []domain.ConventionalInvoice{}
	args := []interface{}{}

	condition := ""
	if createria.Name != "" {
		condition = " conventional_invoices.name = ? "
		args = append(args, createria.Name)
	}

	if createria.Amount != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " conventional_invoices.amount = ? "
		args = append(args, createria.Amount)
	}

	if createria.Tenor != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " conventional_invoices.tenor = ? "
		args = append(args, createria.Tenor)
	}

	if createria.Grade != "" {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " conventional_invoices.grade = ? "
		args = append(args, createria.Grade)
	}

	if createria.Rate != 0 {
		if condition != "" {
			condition = condition + " AND "
		}

		condition = condition + " conventional_invoices.rate = ? "
		args = append(args, createria.Rate)
	}

	if createria.LastID != 0 {
		if condition != "" {
			condition = condition + " AND "
		}
		condition = condition + " conventional_invoices.id = ? "
		args = append(args, createria.LastID)
	}

	query := querySelectConventionalInvoice
	if condition != "" {
		query = query + ` WHERE ` + condition
	}
	query = query + " LIMIT " + strconv.Itoa(pageSize)

	tx, err := u.db.BeginTx(ctx, nil)
	if err != nil {
		return
	}

	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	conventionalInvoice := domain.ConventionalInvoice{}
	for rows.Next() {
		err = rows.Scan(&conventionalInvoice.ID, &conventionalInvoice.Name, &conventionalInvoice.Amount, &conventionalInvoice.Tenor, &conventionalInvoice.Grade, &conventionalInvoice.Rate, &conventionalInvoice.CreatedAt, &conventionalInvoice.UpdatedAt)
		if err != nil {
			return
		}
		conventionalInvoices = append(conventionalInvoices, conventionalInvoice)
	}

	return
}
