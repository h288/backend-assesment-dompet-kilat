module gitlab.com/h288/backend-assement-dompet-kilat/web

go 1.14

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
)
