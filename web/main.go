package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/api"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/config"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/controller"
	mysqlDB "gitlab.com/h288/backend-assement-dompet-kilat/web/database/mysql"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/repository"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

func main() {
	log := log.New(os.Stdout, "Dompet Kilat : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	// database
	db, err := mysqlDB.OpenMySQLConnection()
	if err != nil {
		log.Fatalf("Failed open connection to mysql : %s", err)
		return
	}

	// repository
	userRepository := repository.NewUserMySQLRepostitory(db)
	financingRepository := repository.NewFinancingMySQLRepostitory(db)
	osfRepository := repository.NewOsfMySQLRepostitory(db)
	cInvoiceRepository := repository.NewConventionalInvoiceMySQLRepostitory(db)
	pInvoiceRepository := repository.NewProductiveInvoiceMySQLRepostitory(db)
	reksadanaRepository := repository.NewReksadanaMySQLRepostitory(db)
	sbnRepository := repository.NewSbnMySQLRepostitory(db)

	// usecase
	userUsecase := usecase.NewUserUsecase(userRepository)
	financingUsecase := usecase.NewFinancingUsecase(financingRepository)
	osfUsecase := usecase.NewOsfUsecase(osfRepository)
	cInvoiceUsecase := usecase.NewConventionalInvoiceUsecase(cInvoiceRepository)
	pInvoiceUsecase := usecase.NewProductiveInvoiceUsecase(pInvoiceRepository)
	reksanaUsecase := usecase.NewReksadanaUsecase(reksadanaRepository)
	sbnUsecase := usecase.NewSbnUsecase(sbnRepository)
	authenticationUsecase := usecase.NewAuthenticationUsecase(userRepository)

	// controller
	userController := controller.NewUserController(log, userUsecase)
	financingController := controller.NewFinancingController(log, financingUsecase)
	osfController := controller.NewOsfController(log, osfUsecase)
	cInvoiceController := controller.NewConventionalInvoiceController(log, cInvoiceUsecase)
	pInvoiceController := controller.NewProductiveInvoiceController(log, pInvoiceUsecase)
	reksadanaController := controller.NewReksadanaController(log, reksanaUsecase)
	sbnController := controller.NewSbnController(log, sbnUsecase)
	authenticationController := controller.NewAuthenticationControllerHTTP(authenticationUsecase)

	app := api.NewApp(
		userController,
		financingController,
		osfController,
		cInvoiceController,
		pInvoiceController,
		reksadanaController,
		sbnController,
		authenticationController,
	)
	port := fmt.Sprintf(":%s", os.Getenv(config.WEB_PORT))
	if port == "" {
		port = ":8080"
	}

	server := http.Server{
		Addr:         port,
		Handler:      app,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	serverErrors := make(chan error, 1)
	go func() {
		log.Printf("Listening on port : %s \n", port)
		serverErrors <- server.ListenAndServe()
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)
	select {
	case err := <-serverErrors:
		log.Printf("Failed starting Server : %s", err)
	case <-shutdown:
		log.Printf("Receive shutdown signal : \n")
		timeout := 5 * time.Second
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			log.Printf("Graceful shutdown did not complete in %s get err : %s\n", timeout, err)

			if err := server.Close(); err != nil {
				log.Printf("could not stop server gracefully : %s \n", err)
			}
		}
	}

}
