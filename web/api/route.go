package api

import (
	"net/http"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/controller"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
)

type App struct {
	mux *http.ServeMux
}

func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.HandleCors(&w, r)
	a.mux.ServeHTTP(w, r)
}

func (a *App) HandleCors(w *http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Access-Control-Request-Method") != "" {
		// Set CORS headers
		(*w).Header().Add("Access-Control-Allow-Origin", "*")
		(*w).Header().Add("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS, PUT")
		(*w).Header().Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Token")
		(*w).Header().Add("Content-Type", "application/json; charset=utf-8")
	}
}

func NewApp(
	userController controller.UserControllerHTTP,
	financingController controller.FinancingControllerHTTP,
	osfController controller.OsfControllerHTTP,
	conventionalInvoiceController controller.ConventionalInvoiceControllerHTTP,
	productiveInvoiceController controller.ProductiveInvoiceControllerHTTP,
	reksadanaController controller.ReksadanaControllerHTTP,
	sbnController controller.SbnControllerHTTP,
	authenticationController controller.AuthenticationControllerHTTP,
) http.Handler {
	mux := http.NewServeMux()

	// register
	mux.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			userController.RegisterUser(w, r)
		default:
		}
	})

	// login
	mux.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			authenticationController.Login(w, r)
		default:
		}
	})

	// users
	mux.HandleFunc("/users", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			userController.ListUsers(w, r)
		default:
		}
	})

	// financings
	mux.HandleFunc("/financings", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			financingController.CreateFinancing(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			financingController.ListFinancings(w, r)
		default:
		}
	})

	// osfs
	mux.HandleFunc("/osfs", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			osfController.CreateOsf(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			osfController.ListOsfs(w, r)
		default:
		}
	})

	// conventional invoice
	mux.HandleFunc("/conventional-invoices", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			conventionalInvoiceController.CreateConventionalInvoice(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			conventionalInvoiceController.ListConventionalInvoices(w, r)
		default:
		}
	})

	// productive invoice
	mux.HandleFunc("/productive-invoices", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			productiveInvoiceController.CreateProductiveInvoice(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			productiveInvoiceController.ListProductiveInvoices(w, r)
		default:
		}
	})

	// reksadana
	mux.HandleFunc("/reksadana", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			reksadanaController.CreateReksadana(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			reksadanaController.ListReksadanas(w, r)
		default:
		}
	})

	// reksadana
	mux.HandleFunc("/sbns", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			sbnController.CreateSbn(w, r)
		case http.MethodGet:
			err := authenticationController.ValidateLogin(r)
			if err != nil {
				helper.HTTPResponseErr(w, *err)
				return
			}

			sbnController.ListSbns(w, r)
		default:
		}
	})

	return &App{mux: mux}
}
