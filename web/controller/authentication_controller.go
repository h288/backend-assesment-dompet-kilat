package controller

import (
	"encoding/json"
	"net/http"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

type AuthenticationControllerHTTP interface {
	Login(w http.ResponseWriter, r *http.Request)
	ValidateLogin(r *http.Request) *domain.ResponseErr
}

func NewAuthenticationControllerHTTP(
	authenticationUsecase usecase.AuthenticationUsecase,
) AuthenticationControllerHTTP {
	return &authenticationController{
		authenticationUsecase: authenticationUsecase,
	}
}

type authenticationController struct {
	authenticationUsecase usecase.AuthenticationUsecase
}

func (a *authenticationController) Login(w http.ResponseWriter, r *http.Request) {
	loginData := domain.Login{}
	err := json.NewDecoder(r.Body).Decode(&loginData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	token, apiErr := a.authenticationUsecase.Login(r.Context(), loginData.Username, loginData.Password)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	data := map[string]string{
		"token": token,
	}

	helper.HTTPResponseSuccess(w, http.StatusOK, data)
}

func (a *authenticationController) ValidateLogin(r *http.Request) *domain.ResponseErr {
	token := r.Header.Get("Authorization")
	_, err := a.authenticationUsecase.ValidateLogin(token)
	return err
}
