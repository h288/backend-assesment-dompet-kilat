package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

/*
* SbnControllerHTTP has respontability :
* 1. receive request data and convert that request data to bisnis entity model
* 2. do validation application logic
 */
type SbnControllerHTTP interface {
	CreateSbn(w http.ResponseWriter, r *http.Request)
	ListSbns(w http.ResponseWriter, r *http.Request)
}

func NewSbnController(
	log *log.Logger,
	sbnUsecase usecase.SbnUsecase,
) SbnControllerHTTP {
	return &sbnControllerHTTP{
		sbnUsecase: sbnUsecase,
	}
}

type sbnControllerHTTP struct {
	log        *log.Logger
	sbnUsecase usecase.SbnUsecase
}

func (u *sbnControllerHTTP) CreateSbn(w http.ResponseWriter, r *http.Request) {
	registrationData := domain.ApplicationSbn{}
	err := json.NewDecoder(r.Body).Decode(&registrationData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	newSbn, apiErr := u.sbnUsecase.CreateSbn(r.Context(), registrationData)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, newSbn)
}

func (u *sbnControllerHTTP) ListSbns(w http.ResponseWriter, r *http.Request) {
	qStr := r.URL.Query()
	name := qStr.Get("name")

	amount := 0
	if qStr.Get("amount") != "" {
		amount, _ = strconv.Atoi(qStr.Get("amount"))
	}

	tenor := 0
	if qStr.Get("tenor") != "" {
		tenor, _ = strconv.Atoi(qStr.Get("tenor"))
	}

	typeSbn := qStr.Get("type")

	rate := 0
	if qStr.Get("tenor") != "" {
		rate, _ = strconv.Atoi(qStr.Get("rate"))
	}

	lastID := 0
	if qStr.Get("lastID") != "" {
		lastID, _ = strconv.Atoi(qStr.Get("lastID"))
	}

	pageSize := 15
	if qStr.Get("page_size") != "" {
		pageSize, _ = strconv.Atoi(qStr.Get("page_size"))
	}

	criteria := domain.SbnSearch{
		Name:   name,
		Amount: amount,
		Tenor:  tenor,
		Type:   typeSbn,
		Rate:   rate,
		LastID: lastID,
	}

	sbns, apiErr := u.sbnUsecase.GetSbns(r.Context(), criteria, pageSize)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, sbns)
}
