package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

/*
* FinancingControllerHTTP has respontability :
* 1. receive request data and convert that request data to bisnis entity model
* 2. do validation application logic
 */
type FinancingControllerHTTP interface {
	CreateFinancing(w http.ResponseWriter, r *http.Request)
	ListFinancings(w http.ResponseWriter, r *http.Request)
}

func NewFinancingController(
	log *log.Logger,
	financingUsecase usecase.FinancingUsecase,
) FinancingControllerHTTP {
	return &financingControllerHTTP{
		financingUsecase: financingUsecase,
	}
}

type financingControllerHTTP struct {
	log              *log.Logger
	financingUsecase usecase.FinancingUsecase
}

func (u *financingControllerHTTP) CreateFinancing(w http.ResponseWriter, r *http.Request) {
	registrationData := domain.ApplicationFinancing{}
	err := json.NewDecoder(r.Body).Decode(&registrationData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	newFinancing, apiErr := u.financingUsecase.CreateFinancing(r.Context(), registrationData)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, newFinancing)
}

func (u *financingControllerHTTP) ListFinancings(w http.ResponseWriter, r *http.Request) {
	qStr := r.URL.Query()
	financingName := qStr.Get("financingname")
	sub := qStr.Get("sub")

	count := 0
	if qStr.Get("count") != "" {
		count, _ = strconv.Atoi(qStr.Get("count"))
	}

	lastID := 0
	if qStr.Get("lastID") != "" {
		lastID, _ = strconv.Atoi(qStr.Get("lastID"))
	}

	pageSize := 15
	if qStr.Get("page_size") != "" {
		pageSize, _ = strconv.Atoi(qStr.Get("page_size"))
	}

	criteria := domain.FinancingSearch{
		Name:   financingName,
		Count:  count,
		Sub:    sub,
		LastID: lastID,
	}

	financings, apiErr := u.financingUsecase.GetFinancings(r.Context(), criteria, pageSize)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, financings)
}
