package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

/*
* ReksadanaControllerHTTP has respontability :
* 1. receive request data and convert that request data to bisnis entity model
* 2. do validation application logic
 */
type ReksadanaControllerHTTP interface {
	CreateReksadana(w http.ResponseWriter, r *http.Request)
	ListReksadanas(w http.ResponseWriter, r *http.Request)
}

func NewReksadanaController(
	log *log.Logger,
	reksadanaUsecase usecase.ReksadanaUsecase,
) ReksadanaControllerHTTP {
	return &reksadanaControllerHTTP{
		reksadanaUsecase: reksadanaUsecase,
	}
}

type reksadanaControllerHTTP struct {
	log              *log.Logger
	reksadanaUsecase usecase.ReksadanaUsecase
}

func (u *reksadanaControllerHTTP) CreateReksadana(w http.ResponseWriter, r *http.Request) {
	registrationData := domain.ApplicationReksadana{}
	err := json.NewDecoder(r.Body).Decode(&registrationData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	newReksadana, apiErr := u.reksadanaUsecase.CreateReksadana(r.Context(), registrationData)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, newReksadana)
}

func (u *reksadanaControllerHTTP) ListReksadanas(w http.ResponseWriter, r *http.Request) {
	qStr := r.URL.Query()
	name := qStr.Get("name")

	amount := 0
	if qStr.Get("amount") != "" {
		amount, _ = strconv.Atoi(qStr.Get("amount"))
	}

	returnAmount := 0
	if qStr.Get("return") != "" {
		returnAmount, _ = strconv.Atoi(qStr.Get("return"))
	}

	lastID := 0
	if qStr.Get("lastID") != "" {
		lastID, _ = strconv.Atoi(qStr.Get("lastID"))
	}

	pageSize := 15
	if qStr.Get("page_size") != "" {
		pageSize, _ = strconv.Atoi(qStr.Get("page_size"))
	}

	criteria := domain.ReksadanaSearch{
		Name:   name,
		Amount: amount,
		Return: returnAmount,
		LastID: lastID,
	}

	reksadanas, apiErr := u.reksadanaUsecase.GetReksadanas(r.Context(), criteria, pageSize)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, reksadanas)
}
