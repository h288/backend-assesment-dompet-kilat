package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

/*
* UserControllerHTTP has respontability :
* 1. receive request data and convert that request data to bisnis entity model
* 2. do validation application logic
 */
type UserControllerHTTP interface {
	RegisterUser(w http.ResponseWriter, r *http.Request)
	ListUsers(w http.ResponseWriter, r *http.Request)
}

func NewUserController(
	log *log.Logger,
	userUsecase usecase.UserUsecase,
) UserControllerHTTP {
	return &userControllerHTTP{
		userUsecase: userUsecase,
	}
}

type userControllerHTTP struct {
	log         *log.Logger
	userUsecase usecase.UserUsecase
}

func (u *userControllerHTTP) RegisterUser(w http.ResponseWriter, r *http.Request) {
	registrationData := domain.UserRegistration{}
	err := json.NewDecoder(r.Body).Decode(&registrationData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	newUser, apiErr := u.userUsecase.CreateUser(r.Context(), registrationData)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, newUser)
}

func (u *userControllerHTTP) ListUsers(w http.ResponseWriter, r *http.Request) {
	qStr := r.URL.Query()
	email := qStr.Get("email")
	username := qStr.Get("username")
	lastID := 0
	if qStr.Get("lastID") != "" {
		lastID, _ = strconv.Atoi(qStr.Get("lastID"))
	}

	pageSize := 15
	if qStr.Get("page_size") != "" {
		pageSize, _ = strconv.Atoi(qStr.Get("page_size"))
	}

	criteria := domain.UserSearch{
		Email:    email,
		Username: username,
		LastID:   lastID,
	}

	users, apiErr := u.userUsecase.GetUsers(r.Context(), criteria, pageSize)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, users)
}
