package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/domain"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/helper"
	"gitlab.com/h288/backend-assement-dompet-kilat/web/usecase"
)

/*
* OsfControllerHTTP has respontability :
* 1. receive request data and convert that request data to bisnis entity model
* 2. do validation application logic
 */
type OsfControllerHTTP interface {
	CreateOsf(w http.ResponseWriter, r *http.Request)
	ListOsfs(w http.ResponseWriter, r *http.Request)
}

func NewOsfController(
	log *log.Logger,
	osfUsecase usecase.OsfUsecase,
) OsfControllerHTTP {
	return &osfControllerHTTP{
		osfUsecase: osfUsecase,
	}
}

type osfControllerHTTP struct {
	log        *log.Logger
	osfUsecase usecase.OsfUsecase
}

func (u *osfControllerHTTP) CreateOsf(w http.ResponseWriter, r *http.Request) {
	registrationData := domain.ApplicationOsf{}
	err := json.NewDecoder(r.Body).Decode(&registrationData)
	if err != nil {
		helper.HTTPResponseErr(w, domain.ResponseErr{
			Code:    domain.InternalServerError.Error(),
			Message: err.Error(),
		})
		return
	}

	newOsf, apiErr := u.osfUsecase.CreateOsf(r.Context(), registrationData)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, newOsf)
}

func (u *osfControllerHTTP) ListOsfs(w http.ResponseWriter, r *http.Request) {
	qStr := r.URL.Query()
	name := qStr.Get("name")

	amount := 0
	if qStr.Get("amount") != "" {
		amount, _ = strconv.Atoi(qStr.Get("amount"))
	}

	tenor := 0
	if qStr.Get("tenor") != "" {
		tenor, _ = strconv.Atoi(qStr.Get("tenor"))
	}

	grade := qStr.Get("grade")

	rate := 0
	if qStr.Get("tenor") != "" {
		rate, _ = strconv.Atoi(qStr.Get("rate"))
	}

	lastID := 0
	if qStr.Get("lastID") != "" {
		lastID, _ = strconv.Atoi(qStr.Get("lastID"))
	}

	pageSize := 15
	if qStr.Get("page_size") != "" {
		pageSize, _ = strconv.Atoi(qStr.Get("page_size"))
	}

	criteria := domain.OsfSearch{
		Name:   name,
		Amount: amount,
		Tenor:  tenor,
		Grade:  grade,
		Rate:   rate,
		LastID: lastID,
	}

	osfs, apiErr := u.osfUsecase.GetOsfs(r.Context(), criteria, pageSize)
	if apiErr != nil {
		helper.HTTPResponseErr(w, *apiErr)
		return
	}

	helper.HTTPResponseSuccess(w, http.StatusCreated, osfs)
}
