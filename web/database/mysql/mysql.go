package database

import (
	"database/sql"
	"fmt"
	"os"
	"sync"

	"gitlab.com/h288/backend-assement-dompet-kilat/web/config"
)

var DB *sql.DB
var once sync.Once

func OpenMySQLConnection() (*sql.DB, error) {
	var err error
	once.Do(func() {
		if DB == nil {
			driverName := "mysql"

			uri := fmt.Sprintf(
				"%s:%s@tcp(%s:%s)/%s?parseTime=true",
				os.Getenv(config.MYSQL_USERNAME),
				os.Getenv(config.MYSQL_PASSWORD),
				os.Getenv(config.MYSQL_URI),
				os.Getenv(config.MYSQL_PORT),
				os.Getenv(config.MYSQL_DATABASE),
			)

			DB, err = sql.Open(driverName, uri)
		}
	})

	return DB, err
}
