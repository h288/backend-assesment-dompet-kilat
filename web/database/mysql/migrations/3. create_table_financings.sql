use `backend-assement-dompet-kilat`;

CREATE TABLE financings (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `count` INT,
    `sub` VARCHAR(255) NULL,
    `created_at` TIMESTAMP,
    `updated_at` TIMESTAMP,
    INDEX(created_at, updated_at)
);