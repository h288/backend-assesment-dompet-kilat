use `backend-assement-dompet-kilat`;

CREATE TABLE `reksadana` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `amount` INT NOT NULL,
    `return` INT NOT NULL,
    `created_at` TIMESTAMP,
    `updated_at` TIMESTAMP,
    INDEX(created_at, updated_at)
);