use `backend-assement-dompet-kilat`;

CREATE TABLE users (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `email` VARCHAR(255) NOT NULL UNIQUE,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `created_at` TIMESTAMP,
    `updated_at` TIMESTAMP,
    INDEX(created_at, updated_at)
);