package logic_assement

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGroupDistributeVariants(t *testing.T) {
	arr := []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}

	expectedResult := [][]string{
		[]string{"a", "a", "a"},
		[]string{"b"},
		[]string{"c", "c"},
		[]string{"b", "b", "b"},
		[]string{"d", "d"},
		[]string{"e", "e", "e"},
	}

	results := groupArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from grouping arr test for arr : %s", expectedResult, results, arr)
	}
}

func TestGroupSeparateVariants(t *testing.T) {
	arr := []string{"a", "a", "a", "b", "c", "c"}

	expectedResult := [][]string{
		[]string{"a", "a", "a"},
		[]string{"b"},
		[]string{"c", "c"},
	}

	results := groupArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from grouping arr test for arr : %s", expectedResult, results, arr)
	}
}

func TestGroupSingleVariants(t *testing.T) {
	arr := []string{"a", "a", "a"}

	expectedResult := [][]string{
		[]string{"a", "a", "a"},
	}

	results := groupArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from grouping arr test for arr : %s", expectedResult, results, arr)
	}
}

func TestGroupEmptyVariants(t *testing.T) {
	arr := []string{}

	expectedResult := [][]string{}

	results := groupArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from grouping arr test for arr : %s", expectedResult, results, arr)
	}
}
