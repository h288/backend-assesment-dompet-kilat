package logic_assement

import "testing"

// Odd Length of Characters
func TestValidPallindromeOddCharacterAndInCaseSensitive(t *testing.T) {
	sentence := "AcbCa"
	isCaseSensitive := false
	expectedResult := true

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestInValidPallindromeOddCharacterAndInCaseSensitive(t *testing.T) {
	sentence := "dcbCa"
	isCaseSensitive := false
	expectedResult := false

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestValidPallindromeOddCharacterAndCaseSensitive(t *testing.T) {
	sentence := "ACbCA"
	isCaseSensitive := true
	expectedResult := true

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestInValidPallindromeOddCharacterAndCaseSensitive(t *testing.T) {
	sentence := "AcbCA"
	isCaseSensitive := true
	expectedResult := false

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

// Event length of characters
func TestValidPallindromeEvenCharacterAndInCaseSensitive(t *testing.T) {
	sentence := "AcCa"
	isCaseSensitive := false
	expectedResult := true

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestInValidPallindromeEvenCharacterAndInCaseSensitive(t *testing.T) {
	sentence := "AdCa"
	isCaseSensitive := false
	expectedResult := false

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestValidPallindromeEvenCharacterAndCaseSensitive(t *testing.T) {
	sentence := "ACCA"
	isCaseSensitive := true
	expectedResult := true

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}

func TestInValidPallindromeEvenCharacterAndCaseSensitive(t *testing.T) {
	sentence := "ADCA"
	isCaseSensitive := true
	expectedResult := false

	t.Logf("Test Case For Sentence : %s, case sensitive : %t \n", sentence, isCaseSensitive)

	result := isPalindrome(sentence, isCaseSensitive)
	if expectedResult != result {
		t.Errorf("Test case failed! expected %t, but get %t from palindrome test for sentence : %s", expectedResult, result, sentence)
	}
}
