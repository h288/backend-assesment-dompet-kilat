package logic_assement

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCountArrayDistributeVariants(t *testing.T) {
	arr := []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}

	expectedResult := []Count{
		Count{Total: 3, Character: "a"},
		Count{Total: 1, Character: "b"},
		Count{Total: 2, Character: "c"},
		Count{Total: 3, Character: "b"},
		Count{Total: 2, Character: "d"},
		Count{Total: 3, Character: "e"},
	}

	results := countArray(arr)
	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from count arr test for arr : %+v", expectedResult, results, arr)
	}
}

func TestCountSeparateVariants(t *testing.T) {
	arr := []string{"a", "a", "a", "b", "c", "c"}

	expectedResult := []Count{
		Count{Total: 3, Character: "a"},
		Count{Total: 1, Character: "b"},
		Count{Total: 2, Character: "c"},
	}

	results := countArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from count arr test for arr : %s", expectedResult, results, arr)
	}
}

func TestCountSingleVariants(t *testing.T) {
	arr := []string{"a", "a", "a"}

	expectedResult := []Count{
		Count{Total: 3, Character: "a"},
	}

	results := countArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from count arr test for arr : %s", expectedResult, results, arr)
	}
}

func TestCountEmptyVariants(t *testing.T) {
	arr := []string{}

	expectedResult := []Count{}

	results := countArray(arr)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case failed! expected %v, but get %v from count arr test for arr : %s", expectedResult, results, arr)
	}
}
