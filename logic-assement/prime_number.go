package logic_assement

func findPrimeByRange(start, end int) []int {
	results := []int{}
	for ; start <= end; start++ {
		// special prime number
		if start == 2 {
			results = append(results, 2)
			continue
		}

		// minus
		if start <= 1 || start%2 == 0 {
			continue
		}

		firstPrimeNumber := 2
		if isPrimeNumber(firstPrimeNumber, start) {
			results = append(results, start)
		}
	}

	return results
}

func isPrimeNumber(start, number int) bool {
	if start == 0 {
		start = 2
	}

	if start == number {
		return true
	}

	isNumberEven := number%start == 0
	isNumberLessThanMinPrime := number < start
	if isNumberEven || isNumberLessThanMinPrime {
		return false
	}

	return isPrimeNumber(start+1, number)
}
