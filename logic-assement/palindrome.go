package logic_assement

import "strings"

func isPalindrome(sentence string, isCaseSensitive bool) bool {
	lengthCharacter := len(sentence)
	median := lengthCharacter / 2

	for i := 0; i < median; i++ {
		frontCharacter := string(sentence[i])
		backCharacter := string(sentence[lengthCharacter-(i+1)])

		if !isCaseSensitive {
			frontCharacter = strings.ToLower(frontCharacter)
			backCharacter = strings.ToLower(backCharacter)
		}

		if frontCharacter != backCharacter {
			return false
		}
	}

	return true
}
