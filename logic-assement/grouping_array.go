package logic_assement

func groupArray(arr []string) [][]string {
	results := [][]string{}

	index := 0
	for index < len(arr) {
		initialVal := arr[index]

		tempGrouping := []string{initialVal}
		pointerIndex := index + 1
		for pointerIndex < len(arr) {
			pointerVal := arr[pointerIndex]

			// store temp grouping and reset index
			if pointerVal != initialVal {
				break
			}

			tempGrouping = append(tempGrouping, pointerVal)
			pointerIndex++
		}

		results = append(results, tempGrouping)
		index = pointerIndex
	}

	return results
}
