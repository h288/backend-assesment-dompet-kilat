package logic_assement

type Count struct {
	Total     int
	Character string
}

func countArray(arr []string) []Count {
	results := []Count{}

	index := 0
	for index < len(arr) {
		initialVal := arr[index]

		count := 1
		pointerIndex := index + 1
		for pointerIndex < len(arr) {
			pointerVal := arr[pointerIndex]

			// store temp grouping and reset index
			if pointerVal != initialVal {
				break
			}

			count++
			pointerIndex++
		}

		results = append(results, Count{Total: count, Character: initialVal})

		index = pointerIndex
	}

	return results
}
