package logic_assement

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Check number is prime
func TestValidPrimeNumberFirstPrime(t *testing.T) {
	firstPrimeNumber := 2
	number := 2
	expectedResult := true

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestValidPrimeNumberLessThan10(t *testing.T) {
	firstPrimeNumber := 2
	number := 3
	expectedResult := true

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestValidPrimeNumberLessThan100(t *testing.T) {
	firstPrimeNumber := 2
	number := 37
	expectedResult := true

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestValidPrimeNumber(t *testing.T) {
	firstPrimeNumber := 2
	number := 3
	expectedResult := true

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestInValidPrimeLessThanMinimum(t *testing.T) {
	firstPrimeNumber := 2
	number := 1
	expectedResult := false

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestInValidPrimeEvenNumber(t *testing.T) {
	firstPrimeNumber := 2
	number := 4
	expectedResult := false

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

func TestInValidPrimeOddNumber(t *testing.T) {
	firstPrimeNumber := 2
	number := 9
	expectedResult := false

	result := isPrimeNumber(firstPrimeNumber, number)
	if expectedResult != result {
		t.Errorf("Test case fialed! expected %t, but get %t from number : %d", expectedResult, result, number)
	}
}

// List from prime number

func TestGetListPrimeNumberFromMinusToTen(t *testing.T) {
	startNumber := -1
	endNumber := 10
	expectedResult := []int{2, 3, 5, 7}

	results := findPrimeByRange(startNumber, endNumber)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from start number : %d to number %d", expectedResult, results, startNumber, endNumber)
	}
}

func TestGetListPrimeNumberLessThan10(t *testing.T) {
	startNumber := 0
	endNumber := 10
	expectedResult := []int{2, 3, 5, 7}

	results := findPrimeByRange(startNumber, endNumber)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from start number : %d to number %d", expectedResult, results, startNumber, endNumber)
	}
}

func TestGetListPrimeNumberLessThan20(t *testing.T) {
	startNumber := 0
	endNumber := 20
	expectedResult := []int{2, 3, 5, 7, 11, 13, 17, 19}

	results := findPrimeByRange(startNumber, endNumber)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from start number : %d to number %d", expectedResult, results, startNumber, endNumber)
	}
}

func TestGetListPrimeNumberNotFromZeroLessThan10(t *testing.T) {
	startNumber := 2
	endNumber := 10
	expectedResult := []int{2, 3, 5, 7}

	results := findPrimeByRange(startNumber, endNumber)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from start number : %d to number %d", expectedResult, results, startNumber, endNumber)
	}
}

func TestGetListPrimeNumberNotFromZeroLessThan20(t *testing.T) {
	startNumber := 4
	endNumber := 20
	expectedResult := []int{5, 7, 11, 13, 17, 19}

	results := findPrimeByRange(startNumber, endNumber)

	if !assert.Equal(t, expectedResult, results) {
		t.Errorf("Test case fialed! expected %+v, but get %+v from start number : %d to number %d", expectedResult, results, startNumber, endNumber)
	}
}
